Feature: Scenarios for Shopping cart

@TC002 @Suite1
  Scenario: Scenario to verify laptop added to Shopping cart
    Given I open "https://www.demoblaze.com/index.html" using the browser "CHROME" 
    When I Login to demoblaze with user "usr202103211209060197" and password "pass202103211209060197"
    Then demoblaze welcome page shows user name
    When user goes to "Laptops" Category 
    Then the laptop "Sony vaio i5" is displayed in a card
    When user add the "Sony vaio i5" to cart
    Then Product added alert is displayed with text "Product added."
    