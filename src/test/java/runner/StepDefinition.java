package runner;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pom.demoblaze.DemoBlazeMainLoggedPage;
import pom.demoblaze.DemoblazeInit; 

import architecture.WebDriverManager;
 
 

public class StepDefinition { 
	WebDriverManager selenium;
	DemoblazeInit demoBlazeInit;
	DemoBlazeMainLoggedPage demoBlazeMainLoggedPage;

	 
  
@Given("I open {string} using the browser {string}")
public void i_open_using_the_browser(String baseUrl, String browser) {
	selenium = new WebDriverManager();   
    selenium.getURL(baseUrl,browser); 

    if (baseUrl.contains("demoblaze")) {
    	demoBlazeInit = new DemoblazeInit(selenium.getDriver());
    }
}

 
@When("I signUp in demoblaze with user initial name {string} and password {string}")
public void i_sign_up_in_demoblaze_with_user_initial_name_and_password(String user, String password) {
	demoBlazeInit.SignUp(user, password);
    
}


@Then("SignUp demoblaze alert is displayed with message {string}")
public void sign_up_demoblaze_popup_is_displayed(String expectedSignUpText) {
	demoBlazeInit.verifySignUpSuccessful(expectedSignUpText);
}



@When("I Login to demoblaze with new user credentials")
public void i_login_to_demoblaze_with_new_user_credentials() {
	demoBlazeMainLoggedPage =  demoBlazeInit.loginWithExistingCredentials();
}
 

@Then("demoblaze welcome page shows user name")
public void demoblaze_welcome_page_shows_user_name() {
	demoBlazeMainLoggedPage.verifyWelcomeNameisDisplayed();
}
@When("I Logout from demoblaze")
public void i_logout_from_demoblaze() {
	 demoBlazeMainLoggedPage.LogOut();
}
@Then("demoblaze home page is displayed with sign up link")
public void demoblaze_home_page_is_displayed_with_sign_up_link() {
	demoBlazeInit.verifySignUpLinkDisplayed();
}
 
@When("I Login to demoblaze with user {string} and password {string}")
public void i_login_to_demoblaze_with_user_and_password(String user, String password) {
	demoBlazeMainLoggedPage =  demoBlazeInit.loginWithProvidedCredentials(user,password);
}
 


@When("user goes to {string} Category")
public void user_goes_to_category(String categoryType) {
    demoBlazeMainLoggedPage.goToCategory(categoryType);
}
 
@Then("the laptop {string} is displayed in a card")
public void the_laptop_is_displayed_in_a_card(String laptop) {
    demoBlazeMainLoggedPage.verifyLaptopIsDisplayedInCard(laptop);
}
@When("user add the {string} to cart")
public void user_add_the_to_cart(String item) {
	demoBlazeMainLoggedPage.addItemToCart(item);
}
@Then("Product added alert is displayed with text {string}")
public void product_added_alert_is_displayed_with_text(String text) {
	demoBlazeMainLoggedPage.verifyProductAddedAlert(text);
}
	
	@After
	public void afterScenario() 
	{ 
		selenium.getDriver().close(); 
		selenium.getDriver().quit(); 
		  System.out.println("Thead ID: " +  Thread.currentThread().getId() + " -  driver closed" );
	}

}
