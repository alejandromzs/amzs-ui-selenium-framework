package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( 
		plugin = {"progress","html:target/cucumber-reports.html","json:target/Cucumber-report.json","timeline:target/timeline"},
		features = { "src/test/resources/features"},  
        tags = "@Suite1",
		monochrome = true
		)
public class RunnerTest {

}
