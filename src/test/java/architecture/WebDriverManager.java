package architecture;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverManager {
	WebDriver driver; 
	
	
	public void getURL(String baseUrl, String browser) {
		switch (browser) {
		 case "CHROME":
			 //chrome Driver version
			 String chromeDriverLocation = System.getProperty("user.dir")+"\\lib\\chromedriver89.exe"; 
			 
			 System.setProperty("webdriver.chrome.driver",chromeDriverLocation);
			 driver = new ChromeDriver();
			 //chrome browser Dimension
			 Dimension dimension = new Dimension(1920, 1080); 
			 driver.manage().window().setSize(dimension);
			 //implicit wait
			 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			break;
		 default:
				break;
			}
		
		 driver.get(baseUrl);
	}
	
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	 
}
