package architecture;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	public static String getDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
		Date date = new Date();
		String dateformatted = formatter.format(date); 
		return dateformatted; 
	}
	 	
}
