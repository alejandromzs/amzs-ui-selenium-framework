package pom.demoblaze;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemoBlazeMainLoggedPage {
	WebDriver driver;
	WebDriverWait wait;
	Map<String,String> credentials; 
	
	By navLink_welcome = By.xpath("//a[@class='nav-link'][contains(text(),'Welcome')]");
	By navLink_LogOut = By.xpath("//a[@class='nav-link'][contains(text(),'Log out')]");
	
	String link_category = "//*[@id=\"itemc\"][.='%s']";
	String link_card = "//div[contains(@class,'card')]//h4[.='%s']";
	By btn_addToCart = By.xpath("//a[.='Add to cart']");
	
	public DemoBlazeMainLoggedPage(WebDriver driver, Map<String,String> credentials) {
		this.driver = driver; 
		this.wait = new WebDriverWait(driver, 20);;
		this.credentials = credentials; 
	}

	public void verifyWelcomeNameisDisplayed() {
		wait.until(ExpectedConditions.textToBePresentInElementLocated(navLink_welcome, "Welcome " + credentials.get("user") )); 
	}

	public  void LogOut() {
		driver.findElement(navLink_LogOut).click(); 
		
	}

	public void goToCategory(String categoryType) { 
	    driver.findElement(By.xpath(String.format(link_category, categoryType))).click(); 
	}

	public void verifyLaptopIsDisplayedInCard(String laptop) {
 		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(link_card,laptop ))));
		
	}

	public void addItemToCart(String item) {
		driver.findElement(By.xpath(String.format(link_card,item ))).click();
		wait.until(ExpectedConditions.elementToBeClickable(btn_addToCart));
		driver.findElement(btn_addToCart).click();
		
	}

	public void verifyProductAddedAlert(String expectedText) {
		 wait.until(ExpectedConditions.alertIsPresent());
		 Alert alert = driver.switchTo().alert(); 
		 String actualText = alert.getText();	
		 Assert.assertEquals(expectedText, actualText); 
		 alert.accept(); 
		
	}
	
}
