package pom.demoblaze;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import architecture.Utils;

public class DemoblazeInit {
	WebDriver driver;
	WebDriverWait wait;
	Map<String,String> credentials; 
	
	By menuLink_SignUp = By.xpath("//a[@data-target='#signInModal']"); 
	By SignUp_modal = By.xpath("//div[@id='signInModal']");
	By input_signUp_username = By.xpath("//input[@id='sign-username']");
	By input_signUp_password = By.xpath("//input[@id='sign-password']");
	By btn_SignUp = By.xpath("//button[@type='button'][.='Sign up']");
	
	By menuLink_LogIn = By.xpath("//a[@data-target='#logInModal']");
	By input_LogIn_username = By.xpath("//input[@id='loginusername']");
	By input_LogIn_password = By.xpath("//input[@id='loginpassword']");
	By btn_LogIn  = By.xpath("//button[@type='button'][.='Log in']");
	

	public DemoblazeInit(WebDriver driver) {
		this.driver = driver; 
		this.wait = new WebDriverWait(driver, 20);;
        System.out.println("Thread ID: " +  Thread.currentThread().getId() + " - get Demoblaze driver");
	}

	public void SignUp(String user, String password) { 
		driver.findElement(menuLink_SignUp).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(SignUp_modal)));  
		credentials = addDateToUserAndPassword(user,password); 
		driver.findElement(input_signUp_username).sendKeys(credentials.get("user"));
		driver.findElement(input_signUp_password).sendKeys(credentials.get("password"));
		driver.findElement(btn_SignUp).click(); 
		System.out.println("SignUp with " + credentials.get("user") + " and: " + credentials.get("password") ); 	
	}
	
	
	public Map<String, String> addDateToUserAndPassword(String user, String password) { 
		//get token	
		String token = Utils.getDate(); 
		Map<String,String> map=new HashMap<String,String>(); 
		//Getting not repeated user 
		 user = user + token;
	    //Getting not repeated password 	
		 password = password + token; 
		
		 map.put("user", user);  
		 map.put("password", password);    
		 
		 return map;
	}

	public void verifySignUpSuccessful(String expectedSignUpText) {
		 wait.until(ExpectedConditions.alertIsPresent());
		 Alert alert = driver.switchTo().alert(); 
		 String SignUpActualText = alert.getText();	
		 Assert.assertEquals(expectedSignUpText, SignUpActualText); 
		 alert.accept(); 
		  
		 
	}

	public DemoBlazeMainLoggedPage loginWithExistingCredentials() {
		driver.findElement(menuLink_LogIn).click();
		driver.findElement(input_LogIn_username).sendKeys(credentials.get("user"));
		driver.findElement(input_LogIn_password).sendKeys(credentials.get("password"));
		driver.findElement(btn_LogIn).click();
		
		return new DemoBlazeMainLoggedPage(driver,credentials);
	}

	public void verifySignUpLinkDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(menuLink_SignUp)));  
		
	}

	public DemoBlazeMainLoggedPage loginWithProvidedCredentials(String user, String password) {
		credentials = new HashMap<String, String>();
		credentials.put("user", user);
		credentials.put("password", password);
		
		return loginWithExistingCredentials();
		
	}

}
