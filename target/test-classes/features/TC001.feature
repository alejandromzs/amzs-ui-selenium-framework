Feature: Scenarios for users in demoblaze

@TC001 @Suite1
  Scenario: Scenario to Sign Up, Log In, and Log Out user
    Given I open "https://www.demoblaze.com/index.html" using the browser "CHROME"
    When I signUp in demoblaze with user initial name "usr" and password "pass"
    Then SignUp demoblaze alert is displayed with message "Sign up successful."
    When I Login to demoblaze with new user credentials
    Then demoblaze welcome page shows user name
    When I Logout from demoblaze 
    Then demoblaze home page is displayed with sign up link
    