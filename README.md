#A Java Selenium WedDriver in Parallel


### Pre-requisites
```
  1) Tested with Chrome v. 89
  2) For parallel execution, make sure that Runner end in "Test"
```

###  Steps to run in Parallel
```                                                                      	
  From Console: (mvn clean test -DskipTests=false -Dcucumber.features="src/test/resources/features" -Dcucumber.filter.tags="@Suite1"
  From Run as -> Maven Build -> Goals: clean test -DskipTests=false -Dcucumber.features="src/test/resources/features" -Dcucumber.filter.tags="@Suite1"
```


### Steps to run Sequentially
```
  Go to folder "src/test/java/runner
  Execute as JUnit Test the file Runner.java
```

### Review Results
```
  Go to folder "target"
  Open cucumber-reports.html report with chrome browser 
  Go to folder "target/timeline"
  Open index.html report with chrome browser to show time consumed
```